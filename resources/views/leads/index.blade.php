@extends('templates.template')

@section('content')
    <h1 class="text-center">DC Tecnologias e Mídias Sociais</h1><hr>

    <div class="text-center mt-3 mb-3">
        <a href="{{url('leads/create')}}">
            <button type="button" class="btn btn-success">Cadastrar</button>
        </a>
    </div>

    <div class="col-11 m-auto text-center">
    @csrf

    <form href="{{url('leads.search')}}">
    @method('POST')
    @csrf
        <input type="text" name="search">
        <button type="submit" class="btn btn-secondary">Pesquisar</button>
    </form>

        <table class="table table-dark table-hover table-condensed" id="table_id">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Usuário</th>
                <th scope="col">Site</th>
                <th scope="col">E-mail</th>
                <th width=12%>Telefone</th>
                <th width=25%>Ações</th>
                </tr>
            </thead>
            <tbody>

                @foreach($leads as $lead)
                    @php
                        $user=$lead->find($lead->id)->relUsers; 
                    @endphp
                    <tr>
                        <th scope="row">{{$lead->id}}</th>
                        <td>{{$user->name}}</td>
                        <td>{{$lead->name}}</td>
                        <td>{{$lead->email}}</td>
                        <td>{{$lead->phone}}</td>
                        <td>
                            <a href="{{ url('leads',$lead->id) }}">
                                <button type="button" class="btn btn-primary">Visualizar</button>
                            </a>

                            <a href="{{url('leads.edit',$lead->id)}}">
                                <button type="button" class="btn btn-warning">Editar</button>
                            </a>

                            <a  href="{{url('leads.destroy',$lead->id)}}" class="js-del"> 
                            @method('DELETE')
                                <button onclick="return confirm('Você tem certeza que deseja excluir?');" type="button" class="btn btn-danger">Deletar</button>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table><hr>
        @if (isset($filters))
            {{ $leads->appends($filters)->links() }}
        @else   
            {{ $leads->links() }}
        @endif
    </div>

@endsection