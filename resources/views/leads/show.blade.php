@extends('templates.template')

@section('content')
    <h1 class="text-center">Visualizar</h1><hr>
    @php
            $user=$leads->find($leads->id)->relUsers;
    @endphp

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-right">
                <a href="{{ url('leads') }}">
                    <button type="button" class="btn btn-success" style="margin-top: 15px; margin-left: 15px; margin-right: 15px; ">Voltar</button>
                </a>
            </div>
        </div>
    </div>

    <div style="margin-top: 15px; margin-left: 15px; margin-right: 15px; ">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Nome:</strong>
                    {{ $user->name }}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Site:</strong>
                    {{ $leads->name }}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Email:</strong>
                    {{ $leads->email }}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Telefone:</strong>
                    {{ $leads->phone }}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Criado em:</strong>
                    {{ $leads->created_at }}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Atualizado em:</strong>
                    {{ $leads->updated_at }}
                </div>
            </div>
        </div>
    </div>
@endsection