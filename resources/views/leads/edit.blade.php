@extends('templates.template')

@section('content')

<h1 class="text-center">Editar</h1> <hr>

        @if(isset($errors) && count($errors)>0)
            <div class="text-center mt-4 mb-4 p-2 alert-danger">
                @foreach($errors->all() as $erro)
                    {{$erro}}<br>
                @endforeach
            </div>
        @endif

        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-right">
                    <a href="{{ url('leads') }}">
                        <button type="button" class="btn btn-success" style="margin-top: 15px; margin-left: 15px; margin-right: 15px; ">Voltar</button>
                    </a>
                </div>
            </div>
         </div>

         <div class="col-8 m-auto">
            <form name="formEdit" id="formEdit" method="post" action="{{url('leads.update',$lead->id)}}">
                @csrf
                @method('PUT')
                <input class="form-control" type="text" name="name" id="name" placeholder="Endereço do site:" value="{{$lead->name ?? ' '}}" required><br>
                <select class="form-control" name="id_user" id="id_user" required>
                    <option value="{{$lead->relUsers->id ?? ' '}}">{{$lead->relUsers->name ?? 'Usuário'}}</option>
                    @foreach($users as $user)
                        <option value="{{$user->id}}">{{$user->name}}</option>
                    @endforeach
                </select><br>
                <input class="form-control" type="email" name="email" id="email" placeholder="E-mail:" value="{{$lead->email ?? ' '}}" required><br>
                <input class="form-control" type="text" name="phone" id="phone" placeholder="Telefone:" value="{{$lead->phone ?? ' '}}" required><br>
                <input class="btn btn-primary" type="submit" value="Editar">
            </form>
    </div>
@endsection