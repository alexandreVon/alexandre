<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DC</title>
        <link rel="stylesheet" href="{{url('assets/bootstrap/bootstrap-5.0.0-beta1-dist/css/bootstrap.min.css')}}">
</head>
<body>
    @yield('content')
</body>
</htmld