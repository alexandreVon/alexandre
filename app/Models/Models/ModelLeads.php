<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Model;

class ModelLeads extends Model
{
    protected $table='leads';
    protected $fillable = [
        'name',
        'email',
        'phone',
        'user_id'
    ];

    public function relUsers()
    {
       return $this->hasOne( 'App\Models\User', 'id', 'user_id');
    }
    
}
