<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Models\ModelLeads;
use App\Models\User;

class DcController extends Controller
{

    private $objUser;
    private $objLeads;

    public function __construct()
    {
        $this->objUser=new User();
        $this->objLeads=new ModelLeads();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $leads = ModelLeads::orderBy('id')->simplePaginate(5);
        return view('leads.index', compact("leads"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        return view('leads.create', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $request->validate([
            'name' => 'required',
            'email' => 'required|email:rfc,dns',
            'phone' => 'required',
            'user_id' => 'required'
        ]);

        ModelLeads::create($request->all());

        return redirect('leads')
          ->with('success', 'Project created successfully.');       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('leads.show', [
            'leads' => ModelLeads::findOrFail($id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $lead = ModelLeads::find($id);
        $users = User::all();
        return view('leads.edit', compact('lead', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModelLeads $id)
    {
        ModelLeads::where(['id'=>$id])->update([
            'name'=>$request->name,
            'email'=>$request->email,
            'phone'=>$request->phone,
            'user_id'=>$request->user_id
        ]);

        $id->update($request->all());

        return redirect('leads')
          ->with('success', 'Project created successfully.');    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $del=ModelLeads::destroy($id);
        return redirect('leads');

        //template: <script src="{{url("assets\bootstrap\bootstrap-5.0.0-beta1-dist\js\javascript.js")}}"></script>
        //<a> class="js-del
    }

    public function search(Request $request)
    {
        $filters = $request->except('_token');

        $leads = ModelLeads::where('name', 'LIKE', "%{$request->search}%") 
                                ->onwhere('content', 'LIKE', "%{$request->search}%")
                                ->simplePaginate(5);

        return view('leads.index', compact("leads", "filters"));
    }
}

/*
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/5.0.0-beta1-dist/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
        <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.7/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>

        <script>
            $(document).ready( function () {
                $('#table_id').DataTable({
                    "bPaginate": false,
                    "bFilter": true,
                    "bInfo": false,
                    "search": "Pesquisar"
                });

                $('#table_id').DataTable({
                    "language": {
                    "lengthMenu": "Mostrando _MENU_ registros por página",
                    "zeroRecords": "Nada encontrado",
                    "info": "Mostrando página _PAGE_ de _PAGES_",
                    "infoEmpty": "Nenhum registro disponível",
                    "infoFiltered": "(filtrado de _MAX_ registros no total)",
                    "search": "Pesquisar",
                    "next": "Próximo",
                    "previous": "Anterior"
                    }
                });
            });
        </script>
*/