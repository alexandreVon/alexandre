<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DcController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::resource('leads', [App\Http\Controllers\DcController::class]);

Route::GET('/leads/create', [App\Http\Controllers\DcController::class, 'create'])->name('leads.create');
Route::GET('/leads', [App\Http\Controllers\DcController::class, 'index']);
Route::GET('/leads/{id}', [App\Http\Controllers\DcController::class, 'show']);
Route::POST('/leads.store', [App\Http\Controllers\DcController::class, 'store']);
Route::GET('/leads.edit/{id}', [App\Http\Controllers\DcController::class, 'edit']);
Route::PUT('/leads.update/{id}', [App\Http\Controllers\DcController::class, 'update']);
Route::GET('/leads.destroy/{id}', [App\Http\Controllers\DcController::class, 'destroy']);
Route::any('/leads.search', [App\Http\Controllers\DcController::class, 'search']);
